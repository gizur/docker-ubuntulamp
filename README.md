Build the image: `docker build -t ubuntulamp .`

Starting the server this way makes it possible to connect and reconnect to the
server:

    docker run -t -i -p 8080:80 \
    --restart="on-failure:10" --name lampX -h lampX \
    ubuntulamp /bin/bash -c "supervisord; bash"

Exit the server with `ctrl-p` `ctrl-q`. Reconnect with `docker attach logserver`


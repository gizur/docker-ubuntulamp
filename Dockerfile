FROM ubuntu:trusty

# Mirros: http://ftp.acc.umu.se/ubuntu/ http://us.archive.ubuntu.com/ubuntu/
#RUN echo "deb http://ftp.acc.umu.se/ubuntu/ trusty-updates main restricted" > /etc/apt/sources.list

RUN apt-get update
RUN apt-get install -y wget nano curl git telnet rsyslog
ADD ./etc-rsyslog.conf /etc/rsyslog.conf


RUN apt-get install -y apache2
RUN apt-get install -y apache2 php5 php5-curl php5-mysql php5-mcrypt php5-gd
RUN php5enmod mcrypt
RUN a2enmod rewrite
RUN apt-get install -y mysql-server

RUN apt-get install -y python python-setuptools
RUN easy_install supervisor
ADD ./etc-supervisord.conf /etc/supervisord.conf
ADD ./etc-supervisor-conf.d-supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN mkdir -p /var/log/supervisor/
